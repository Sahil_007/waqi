import scrapy
import functools
import json
from waqi.items import WaqiItem
from bs4 import BeautifulSoup
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
class WaqiSpider(scrapy.Spider):
	name="waqi"
	url_list=list()
	for i in range(18):
		url_list.append("https://api.waqi.info/mapq/block/"+str(i)+"/500/")
	start_urls=url_list


	def parse(self,response):
		data=json.loads(response.body)
		data_list=data['cities']
		for each in data_list:
			item=WaqiItem()
			item['city']=each['n'].encode("utf-8")
			item['lat']=each['g'][0]
			item['lng']=each['g'][1]
			item['airQualityIndex']=each['a']
			item['url']=response.url
			item['pollution']={"amount":"","unit":""}
			yield item